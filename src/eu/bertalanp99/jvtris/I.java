package eu.bertalanp99.jvtris;

/**
 * The 'I' shaped Tetromino.
 *
 * @author bertalanp99
 * @version 0.0.1
 * @since 0.0.1
 */
final class I extends Tetromino {
	private static final Colour colour = Colour.CYAN;

	private static final byte[][] NORTH = new byte[][]
	{
        {1, 1, 1, 1},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0}
	};

	private static final byte[][] EAST = new byte[][]
	{
        {0, 1, 0, 0},
        {0, 1, 0, 0},
        {0, 1, 0, 0},
        {0, 1, 0, 0}
	};

	private static final byte[][] SOUTH = NORTH;

	private static final byte[][] WEST = EAST;

	public I(int width)
	{
		super(colour,
				new TetroMatrix(width, NORTH),
				new TetroMatrix(width, EAST),
				new TetroMatrix(width, SOUTH),
				new TetroMatrix(width, WEST));
	}
}
