package eu.bertalanp99.jvtris;

import static com.esotericsoftware.minlog.Log.*;

import picocli.CommandLine;
import picocli.CommandLine.Option;

import javax.swing.JFrame;

import java.util.Set;
import java.util.HashSet;

/**
 * Main class for <code>jvtris</code>.
 *
 * The purpose of this class is to parse command line options and launch a new
 * game. It creates its own thread.
 *
 * @version 0.1
 * @since 0.1
 * @author bertalanp99
 */
class JVtris implements Runnable {
	@Option(names = { "-v", "--verbose" }, description =
	"Be more verbose. Add more apply multiple times to further increase " +
	"verbosity.")
	private boolean[] verbose = new boolean[0];

	/**
	 * When one wants to run this <code>Runnable</code> class, they are supposed
	 * to call this method.
	 */
	public void run()
	{
		if (verbose.length > 0)
		{
			INFO();
		}

		if (verbose.length > 1)
		{
			DEBUG();
		}

		if (verbose.length > 2)
		{
			TRACE();
		}

		this.newGame();
	}

	/**
	 * Creates a new game by creating the common 7 tetrominoes, adding them to a
	 * new <code>PlayField</code> instance and then creating a new
	 * <code>JFrame</code> for the game and starting it.
	 */
	public void newGame()
	{
		PlayField pf = new PlayField();

		Set<Tetromino> tetroSet = new HashSet<Tetromino>();
		tetroSet.add(new I(10));
		tetroSet.add(new J(10));
		tetroSet.add(new L(10));
		tetroSet.add(new O(10));
		tetroSet.add(new S(10));
		tetroSet.add(new T(10));
		tetroSet.add(new Z(10));

		for (Tetromino t : tetroSet)
		{
			pf.addTetromino(t);
		}

		JVtrisFrame frame = new JVtrisFrame(pf);
		frame.startGame();
	}

	/**
	 * Main method that only runs this <code>Runnable</code> class.
	 *
	 * @param args the input arguments
	 */
	public static void main(String[] args)
	{
		CommandLine.run(new JVtris(), args);
	}
}
