package eu.bertalanp99.jvtris;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import java.io.Serializable;

/**
 * Class for keeping the leaderboards.
 *
 * @version 0.1
 * @since 0.1
 * @author bertalanp99
 */
class LeaderBoard implements Serializable {
	/**
	 * Internal class that represents a leaderboard entry.
	 *
	 * @version 0.1
	 * @since 0.1
	 * @author bertalanp99
	 */
	private final class LeaderBoardEntry implements Serializable {
		/**
		 * The name of the player with the score.
		 */
		private final String name;

		/**
		 * The score of the player.
		 */
		private final int score;

		private static final long serialVersionUID = 0L;

		/**
		 * Constructor for a new leaderboard entry.
		 *
		 * @param n the name of the player
		 * @param s the score of the player
		 */
		private LeaderBoardEntry(String n, int s)
		{
			this.name = n;
			this.score = s;
		}

		/**
		 * Getter for the player's score.
		 *
		 * @return the player's score
		 */
		private int getScore()
		{
			return this.score;
		}

		/**
		 * Getter for the player's name.
		 *
		 * @return the player's name
		 */
		private String getName()
		{
			return this.name;
		}

	}

	/**
	 * Constant for the maximal length of the leaderboards.
	 */
	private static final int SIZE = 10;

	private static final long serialVersionUID = 0L;

	/**
	 * The leaderboard itself.
	 */
	private List<LeaderBoardEntry> board = new ArrayList<LeaderBoardEntry>();

	/**
	 * Getter for the leaderboard's length.
	 *
	 * @return the length of the leaderboard
	 */
	public int size()
	{
		return this.board.size();
	}

	/**
	 * Getter for the <code>n</code>-th leaderboard entry's score.
	 *
	 * @param n which entry to get
	 *
	 * @return the score
	 */
	public int getScore(int n)
	{
		return this.board.get(n).getScore();
	}

	/**
	 * Getter for the <code>n</code>-th leaderboard entry's name.
	 *
	 * @param n which entry to get
	 *
	 * @return the name
	 */
	public String getName(int n)
	{
		return this.board.get(n).getName();
	}

	/**
	 * Adds a new record to the leaderboards, if it is eligible.
	 *
	 * @param name the player's name
	 * @param score the player's score
	 */
	public void addScore(String name, TetroScore score)
	{
		int scoreVal = score.getScore();

		if (this.board.size() == SIZE)
		{
			if (scoreVal > this.board.get(SIZE - 1).getScore())
			{
				this.board.remove(SIZE - 1);
			}
			else
			{
				return;
			}
		}

		this.board.add(new LeaderBoardEntry(name, scoreVal));
		Collections.sort(this.board, new Comparator<LeaderBoardEntry>() {
			@Override
			public int compare(LeaderBoardEntry a, LeaderBoardEntry b)
			{
				if (a.getScore() > b.getScore())
				{
					return -1;
				}
				else if (a.getScore() < b.getScore())
				{
					return +1;
				}
				else
				{
					return 0;
				}
			}
		});
	}

	/**
	 * Clears the leaderboards.
	 */
	public void clear()
	{
		this.board.clear();
	}
}
