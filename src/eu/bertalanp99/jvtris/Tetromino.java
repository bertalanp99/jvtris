package eu.bertalanp99.jvtris;

/**
 * Abstract Tetromino class to be extended for all sorts of tetrominoes.
 *
 * @author: bertalanp99
 * @version: 0.1
 * @since 0.1
 */
abstract class Tetromino {
	/**
	 * Enumeration for different (4) possible orientations.
	 *
	 * @see #orientation
	 * @see #rotateCW()
	 * @see #rotateCCW()
	 */
	private enum Orientation {
		NORTH,
		EAST,
		SOUTH,
		WEST;

		public Orientation cycleCW()
		{
			switch (this)
			{
				case NORTH:
					return EAST;

				case EAST:
					return SOUTH;

				case SOUTH:
					return WEST;

				case WEST:
					return NORTH;

				default:
					throw new RuntimeException("It is logically impossible " +
							"to reach this point; please contact dev about " +
							"this.");
			}
		}

		public Orientation cycleCCW()
		{
			switch (this)
			{
				case NORTH:
					return WEST;

				case EAST:
					return NORTH;

				case SOUTH:
					return EAST;

				case WEST:
					return SOUTH;

				default:
					throw new RuntimeException("It is logically impossible " +
							"to reach this point; please contact dev about " +
							"this.");
			}
		}
	}

	/**
	 * Enumeration of Tetromino colours.
	 */
	protected static enum Colour {
		CYAN   ((byte) 'c', 0,   199, 199), // I
		BLUE   ((byte) 'b', 0,   0,   255), // J
		ORANGE ((byte) 'o', 255, 165, 0  ), // L
		YELLOW ((byte) 'y', 255, 255, 0  ), // O
		GREEN  ((byte) 'g', 0,   255, 0  ), // S
		PURPLE ((byte) 'm', 128, 0,   128), // T
		RED    ((byte) 'r', 255, 0,   0  ); // Z

		private final byte colourCode;

		private final java.awt.Color colour;

		private Colour(byte code, int red, int green, int blue)
		{
			this.colourCode = code;
			this.colour = new java.awt.Color(red, green, blue);
		}

		public byte getColourCode()
		{
			return this.colourCode;
		}

		public java.awt.Color getColour()
		{
			return this.colour;
		}

		public static java.awt.Color getColourFromCode(byte code)
		{
			for (Colour c: values())
			{
				if (c.colourCode == code)
				{
					return c.getColour();
				}
			}

			return java.awt.Color.BLACK;
		}
	}

	/**
	 * The Tetromino's colour.
	 *
	 * A constant which takes its value from the enumeration {@link #Colour}.
	 *
	 * @see #Colour
	 */
	protected final Colour colour;

	/**
	 * The width of the playfield the Tetromino is designed for.
	 */
	private final int playFieldWidth;

	/**
	 * The current orientation of the Tetromino.
	 *
	 * Values are taken from the enumeration {@link #Orientation}.
	 *
	 * Orientations can be cycled using {@link #rotateCW()} and {@link
	 * #rotateCCW}.
	 *
	 * @see #Orientation
	 * @see #rotateCW()
	 * @see #rotateCCW()
	 */
	private Orientation orientation = Orientation.NORTH;

	/**
	 * What row to return when {@link #nextRow()} is called.
	 *
	 * This simple counter keeps track of what row shall be returnet upon a
	 * request. It decrements after each request and finally reaches the value
	 * of <code>-1</code> where it stagnates and null references are returned
	 * instead of rows upon further queries.
	 *
	 * Defaults to <code>3</code>, as the Tetrominos all fit into a 4 by 4
	 * matrix by definition (see specification): therefore the index of the last
	 * row is (4 - 1), which is 3. This also implies that the rows of matrices
	 * are returned from bottom to top (logically, as Tetrominos 'fall' from the
	 * top of the playfield).
	 *
	 * @see #nextRow()
	 * @see #Tetromino()
	 */
	private int currentRow;

	/**
	 * The north orientation of the tetromino.
	 */
	private final TetroMatrix north;

	/**
	 * The east orientation of the tetromino.
	 */
	private final TetroMatrix east;

	/**
	 * The south orientation of the tetromino.
	 */
	private final TetroMatrix south;

	/**
	 * The west orientation of the tetromino.
	 */
	private final TetroMatrix west;

	/**
	 * Constructor for a new tetromino
	 *
	 * @param colour the desired colour
	 * @param north the TetroMatrix for the north orientation
	 * @param east the TetroMatrix for the east orientation
	 * @param south the TetroMatrix for the south orientation
	 * @param west the TetroMatrix for the west orientation
	 *
	 * @return 
	 */
	public Tetromino(Colour colour, TetroMatrix north, TetroMatrix east,
			TetroMatrix south, TetroMatrix west)
	{
		if
		(
			(east.size() != north.size())  ||
			(south.size() != north.size()) ||
			(west.size() != north.size())
		)
		{
			throw new IllegalArgumentException("All tetromino arrays must be " +
					"designed for the same playfield size.");
		}

		this.colour = colour;
		this.playFieldWidth = north.size();

		this.north = north;
		this.east = east;
		this.south = south;
		this.west = west;

		this.north.setByte(this.colour.getColourCode());
		this.east.setByte(this.colour.getColourCode());
		this.south.setByte(this.colour.getColourCode());
		this.west.setByte(this.colour.getColourCode());
	}

	/**
	 * Internal method used to get he n-th row of the Tetromino
	 *
	 * @param n the index of the requested row
	 *
	 * @return the row at index {@link #n}
	 */
	private TetroArray nthRow(int n)
	{
		if (n < 0 || n >= 4) // having 4 rows is hardcoded
		{
			throw new IndexOutOfBoundsException("Index out of bounds (should " +
					" be at least 0 and less than 4).");
		}

		switch (this.orientation)
		{
			case NORTH:
				return this.north.getNthRow(n);

			case EAST:
				return this.east.getNthRow(n);

			case SOUTH:
				return this.south.getNthRow(n);

			case WEST:
				return this.west.getNthRow(n);

			default:
				throw new RuntimeException("It is logically impossible to " +
						"reach this point; please contact dev about this.");
		}
	}

	/**
	 * Returns the next row of the tetromino's TetroMatrix (with the current
	 * orientation)
	 *
	 * @return the next row
	 */
	public TetroArray nextRow()
	{
		if (this.currentRow == -1)
		{
			return null;
		}
	
		if (this.currentRow == (4 - 1))
		{
			this.currentRow = this.lastNonZeroRowIndex();
		}

		return this.nthRow(this.currentRow--);
	}

	/**
	 * Returns the index of the last non-empty row of the tetromino.
	 *
	 * @return the index
	 */
	private int lastNonZeroRowIndex()
	{
		int i = (4 - 1);
		while ((i > 0) && this.nthRow(i).isZero())
		{
			--i;
		}

		return i;
	}

	/**
	 * Returns the row that has the first nonzero element on the closest to the
	 * 'left hand side'.
	 *
	 * @return the row
	 */
	public TetroArray leftmostRow()
	{
		int row = 0;
		for (int i = 0; i < 4; ++i)
		{
			int bestSoFar = this.nthRow(row).firstNonZeroElementIndex();
			int current = this.nthRow(i).firstNonZeroElementIndex();
			if ((bestSoFar == -1) || ((current != -1) && (current < bestSoFar)))
			{
				row = i;
			}
		}

		return this.nthRow(row);
	}

	/**
	 * Returns the row that has the first nonzero element on the closest to the
	 * 'right hand side'.
	 *
	 * @return the row
	 */
	public TetroArray rightmostRow()
	{
		int row = 0;
		for (int i = 0; i < 4; ++i)
		{
			int bestSoFar = this.nthRow(row).lastNonZeroElementIndex();
			int current = this.nthRow(i).lastNonZeroElementIndex();
			if ((bestSoFar == -1) || ((current != -1) && (current > bestSoFar)))
			{
				row = i;
			}
		}

		return this.nthRow(row);
	}

	/**
	 * Resets the row counter.
	 */
	public void resetRow()
	{
		this.currentRow = (4 - 1);
	}

	/**
	 * Resets orientation to <code>north</code>.
	 */
	public void resetOrientation()
	{
		this.orientation = Orientation.NORTH;
	}

	/**
	 * Resets both the row counter and the orientation.
	 */
	public void reset()
	{
		this.resetRow();
		this.resetOrientation();
	}

	/**
	 * Rotates the tetromino clockwise.
	 */
	public void rotateCW()
	{
		this.orientation = this.orientation.cycleCW();
	}

	/**
	 * Rotates the tetromino counter-clockwise.
	 */
	public void rotateCCW()
	{
		this.orientation = this.orientation.cycleCCW();
	}
}


