package eu.bertalanp99.jvtris;

import org.junit.Test;
import org.junit.Assert;

class TetroArrayTest {
	@Test
	public void testSize()
	{
		TetroArray foo = new TetroArray(10);
		Assert.assertEquals(foo, 10);
	}

	@Test
	public void testGetNth()
	{
		byte[] arr = new byte[] { 1, 0, 0, 1 };
		TetroArray foo = new TetroArray(6, arr);
		Assert.assertEquals(foo.getNth(0), 0);
		Assert.assertEquals(foo.getNth(1), 1);
	}

	@Test
	public void testShiftLeft()
	{
		byte[] arr = new byte[] { 0, 1, 1, 0, 1 };
		TetroArray foo = new TetroArray(5, arr);
		foo = foo.shiftLeft();
		Assert.assertEquals(foo.getNth(0), 1);
		foo = foo.shiftLeft();
		Assert.assertEquals(foo, null);
	}

	@Test
	public void testShiftRigth()
	{
		byte[] arr= new byte[] { 1, 1, 0, 1, 0 };
		TetroArray foo = new TetroArray(5, arr);
		foo = foo.shiftRight();
		Assert.assertEquals(foo.getNth(0), 0);
		foo = foo.shiftRight();
		Assert.assertEquals(foo, null);
	}

	@Test
	public void testShift()
	{
		byte[] arr = new byte[] { 0, 1, 1, 1, 0 };
		TetroArray foo = new TetroArray(5, arr);
		foo = foo.shift(-1);
		Assert.assertEquals(foo.getNth(0), 1);
		foo = foo.shift(+2);
		Assert.assertEquals(foo.getNth(1), 0);
	}

	@Test
	public void testIsLeftPadded()
	{
		byte[] a = new byte[] { 0, 1, 1 ,1 };
		byte[] b = new byte[] { 1, 0 ,1 ,0 };
		TetroArray foo = new TetroArray(4, a);
		TetroArray bar = new TetroArray(4, b);
		Assert.assertEquals(foo.isLeftPadded(), true);
		Assert.assertEquals(bar.isLeftPadded(), false);
	}

	@Test
	public void testIsRightPadded()
	{
		byte[] a = new byte[] { 0, 1, 1 ,1 };
		byte[] b = new byte[] { 1, 0 ,1 ,0 };
		TetroArray foo = new TetroArray(4, a);
		TetroArray bar = new TetroArray(4, b);
		Assert.assertEquals(foo.isRightPadded(), false);
		Assert.assertEquals(bar.isRightPadded(), true);
	}
}
