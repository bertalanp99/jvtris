package eu.bertalanp99.jvtris;

import org.junit.Test;
import org.junit.Assert;

class TetroScoreTest {
	@Test
	public void testGetScore()
	{
		TetroScore foo = new TetroScore();
		foo.line(1);

		Assert.assertEquals(foo.getScore(), 100);
	}
}
